# LibreHealth Toolkit App
LibreHealth Toolkit App (lh-toolkit-app) assembles all the components from  https://gitlab.com/librehealth/lh-toolkit-webcomponents into a polymer progressive web app. 
These components implement FHIR Resources and GET/POST data using FHIR payload structures using web components and are part of git sub-module in this repo.
This app is built using Polymer Progressive Web App starter kit - using redux template. It has features of responsive layout and application theming.</br>

##SET UP
Instructions for setting up the application:
* Make sure git is correctly installed by checking its version `git --version`.
* Make sure node.js is correctly installed by checking its version `node --version`. Node 8x and 10x are supported versions.
* Make sure npm is correctly installed by checking its version `npm --version`.
* Make sure you have polymer cli installed. Else install it  using `npm i -g polymer-cli`.
* Git clone the app at gitlab recursively by `git clone --recursive https://gitlab.com/parumenon.pm/lh-toolkit-app.git`. This creates a new folder with
lh-toolkit-app. Move into that folder by `cd lh-toolkit-app`.
* Perform `lerna bootstrap --use-workspaces`.
* To run the deployment server and open a browser pointing to its URL, `polymer serve --open`.
Runs on Chrome and Firefox.
* Use your LibreHealth credentials to login else use:
username: admin
password: Admin123</br>
  
##Demo </br> 
Please find the demo at 
<a href="https://parumenon.pm.gitlab.io/lh-toolkit-app/">Demo</a>