/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from '@polymer/lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import '@lh-toolkit/fhir-create-patient/fhir-create-patient.js';
class MyView1 extends PageViewElement {
  _render(props) {
    return html`
      <style>
      .card{
      width: 100%;
      padding: 2px;
      margin: 0;
      background: orange;
      }
      .create{
      width: 100%;
      padding: 2px;
      margin: 0;
      background: #fff5e6;
      }
      </style>
   
      ${SharedStyles}
       <div class="card">
       <b>REGISTER PATIENT:</b>
       </div>
       </br>
       <div class="create">
       <fhir-create-patient url="http://hapi.fhir.org/baseDstu3"></fhir-create-patient>
       </div>
    `;
  }
}

window.customElements.define('my-view1', MyView1);
